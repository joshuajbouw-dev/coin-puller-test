import Sequelize from 'sequelize';
import casual from 'casual';
import _ from 'lodash';
import rp from 'request-promise';

const db = new Sequelize('postgres://joshuajbouw@localhost:5432/blog_dev');

// const AuthorModel = db.define('author', {
//   firstName: { type: Sequelize.STRING },
//   lastName: { type: Sequelize.STRING },
// });

// const PostModel = db.define('post', {
//   title: { type: Sequelize.STRING },
//   text: { type: Sequelize.STRING }
// });

const CoinModel = db.define('coin', {
  name: { type: Sequelize.STRING },
  symbol: { type: Sequelize.STRING }
});

const CoinStatModel = db.define('coinstat', {
  price_usd: { type: Sequelize.STRING },
  price_btc: { type: Sequelize.STRING },
  market_cap_usd: { type: Sequelize.STRING },
  available_supply: { type: Sequelize.STRING },
  total_supply: { type: Sequelize.STRING }
})

CoinModel.hasMany(CoinStatModel);
CoinStatModel.belongsTo(CoinModel);

// AuthorModel.hasMany(PostModel);
// PostModel.belongsTo(AuthorModel);

// Old test data.
// casual.seed(123);
// db.sync({ force: true }).then(() => {
//   _.times(10, () => {
//     return AuthorModel.create({
//       firstName: casual.first_name,
//       lastName: casual.last_name
//     }).then((author) => {
//       return author.createPost({
//         title: `A post by ${author.firstName}`,
//         text: casual.sentences(3)
//       });
//     });
//   });
// });

// This below works.
// db.sync({ force: true }).then(() => {
//   return rp('https://api.coinmarketcap.com/v1/ticker/')
//     .then((res) => JSON.parse(res))
//     .then((res) => {
//       let i;
//       for (i = 0; i < 100; i++) {
//         CoinModel.create({
//           name: res[i].name,
//           symbol: res[i].symbol
//         })
//       };
//     });
// });

db.sync({ force: false }).then(() => {
  return rp('https://api.coinmarketcap.com/v1/ticker/')
    .then(res => JSON.parse(res))
    .then(res => {
      
      Array.from(Array(100).keys()).forEach(i => {
        let coin = res[i];

        // Find one
        CoinModel.findOne({ where: {symbol: coin.symbol} }).then( data => {
          if (!!data) { 
             console.log(`Coin ${coin.name} found. ${data}`);
             
          } else {
            // If there isn't a coin found. Create coin.
            console.log(`Coin ${coin.name} not found`);
            
            CoinModel.create({
              name: coin.name,
              symbol: coin.symbol
            })
          }
        })
      })
    })
    .catch(err => console.log("something went wrong ", err))
    .finally( () => console.log("Output something"))
});

const Author = db.models.author;
const Post = db.models.post;
const Coin = db.models.coin;
const CoinStat = db.models.coinstat;

// const FortuneCookie = {
//   getOne() {
//     return rp('https://api.coinmarketcap.com/v1/ticker/')
//       .then((res) => JSON.parse(res))
//       // .then((res) => {
//       //   return res[0].id;
//       // })
//       .then((res) => {
//         return ({
//           id: id,
//           price_usd: price_usd
//         })
//       });
//   },
// };

export { Author, Post };